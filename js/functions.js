var $win = $(window);
var $nav = $('.nav');
var $buttonMenu = $('.js-btn-menu');
var $menuDropdown = $('.nav ul ul');
var dateFormat = 'dd/mm/yy';
var $sliderProduct = $('.js-slider-product');
var $sliderMain = $('.js-slider-main');
var $tooltip = $('.js-tooltip');
var $langTrigger = $('.js-lang-trigger');
var $popupTrigger = $('.js-popup');
var $jsScrollTo = $('.js-scroll-to');

// Manage mobile menu
$buttonMenu.on('click', function(event) {
	event.preventDefault();

	$(this).toggleClass('active');
	$nav.toggleClass('open');
});

$menuDropdown.each(function() {
	$(this)
		.closest('li')
		.addClass('js-has--dd');
});

var $hasDD = $('.js-has--dd > a');

$hasDD.on('click', function(event) {
	event.preventDefault();

	$(this)
		.siblings('ul')
		.toggleClass('open')
			.closest('li')
			.siblings()
			.find('.open')
			.removeClass('open');
});

// Manage Datepicker
function getDate(element) {
	var date = null;

	try {
		date = $.datepicker.parseDate(dateFormat, element.value);
	} catch(error) {
		date = null
	}

	return date;
}

function startDatepicker() {
	var $datepicker = $('.datepicker:not(.hasDatepicker)');
	var $datepickerFrom = $('.datepicker-from:not(.hasDatepicker)');

	$datepicker.datepicker({
		dateFormat: dateFormat,
		dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
	});

	$datepickerFrom.each(function() {
		var $thisFrom = $(this);
		var $thisTo = $($thisFrom.data('class'));

		$thisFrom.datepicker({
			dateFormat: dateFormat,
			dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			minDate: 0,
			firstDay: 1
		})
		.on('change', function() {
			$thisTo.datepicker('option', 'minDate', getDate(this));
		});

		$thisTo
		.datepicker({
			dateFormat: dateFormat,
			dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			minDate: 0,
			firstDay: 1
		})
		.on('change', function() {
			$thisFrom.datepicker('option', 'maxDate', getDate(this));
		});

		$thisFrom.datepicker('setDate', new Date());
		$thisTo.datepicker('setDate', new Date());
	});
}

startDatepicker();

// Manage slider
if ($sliderProduct.length) {
	$sliderProduct.lightSlider({
		gallery: true,
		item: 1,
		loop: true,
		prevHtml: '<i class="fas fa-angle-double-left"></i>',
		nextHtml: '<i class="fas fa-angle-double-right"></i>',
		thumbItem: 11,
		slideMargin: 0,
		enableDrag: false,
		currentPagerPosition: 'left',
		responsive : [
			{
				breakpoint: 767,
				settings: {
					thumbItem: 6
				}
			}, {
				breakpoint: 479,
				settings: {
					thumbItem: 4,
					controls: false
				}
			}
		]
	});
}

if ($sliderMain.length) {
	$sliderMain.lightSlider({
		item: 1,
		loop: true,
		auto: true,
		speed: 1000,
		slideMargin: 0,
		enableDrag: false,
		controls: false,
		pause: 3000
	});
}

// Manage tooltip
$tooltip.on('click', function(event) {
	event.preventDefault();

	$(this).toggleClass('open');
});

// Manage languages menu
$langTrigger.on('click', function(event) {
	event.preventDefault();

	$(this)
		.closest('.lang')
		.toggleClass('expand');
});

// Manage popups
$popupTrigger.magnificPopup({
	type: 'inline',
	callbacks: {
		open: function() {
			startDatepicker()
		}
	}
})

// Manage scroll to section
$jsScrollTo.on('click', function(event) {
	event.preventDefault();

	var href = $(this).attr('href');

	$('html, body').animate({
		scrollTop: $(href).offset().top
	}, 800)
});
